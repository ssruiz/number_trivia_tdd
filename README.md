# Trivia Number Using TDD and Clean Architecture

A Flutter project to experiment with the TDD and Clean Architechture Design structure.

The app calls the API [NumbersAPI](http://numbersapi.com/#42) in two possible ways:
- GetConcreteNumberTrivia : that returns a trivia for a number the user input
- GetRandomNumberTrivia: get a trivia for a random number; 
